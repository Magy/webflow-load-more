$(document).ready(function () {
    var i = 0;
    $(".w-pagination-next").bind('click', function (e) {
        e.preventDefault();
        $.get($(this).attr('href'), function (data) {
            i++;
            console.log(i);
        })
        .done(function(data) {
            $('.w-dyn-items').append($(data).find('.w-dyn-items').children('.w-dyn-item'));
            var next = $(data).find('.w-pagination-next');
            if (next.length > 0)
                $('.w-pagination-next').attr('href', next.attr('href'));
            else
                $('.w-pagination-next').remove();
        });
    });
});
